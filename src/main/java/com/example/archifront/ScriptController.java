package com.example.archifront;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;


@Component
public class ScriptController {
    private final OrderService orderService;
    private static final Logger log = LoggerFactory.getLogger(ScriptController.class);

    public ScriptController(OrderService orderService) {
        this.orderService = orderService;
    }

    @Scheduled(cron = "*/10 * * * * *")
    public void printAllNotDoneOrders() throws IOException {
        Order[] orders = this.orderService.getNotDoneOrders();

        FileWriter fileWriter = new FileWriter(new Date().toString());
        PrintWriter printWriter = new PrintWriter(fileWriter);
        printWriter.printf("ORDERS RECAP AT %s\n", new Date().toString());

        for (Order order :
                orders) {
            printWriter.printf("==========================================\n");
            printWriter.printf("%s\n", order.toString());
            printWriter.printf("==========================================\n\n");

        }


        printWriter.close();
    }
}


