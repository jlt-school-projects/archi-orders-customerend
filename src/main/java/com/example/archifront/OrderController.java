package com.example.archifront;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
public class OrderController {

    private final ProductsService productsService;
    private final OrderService orderService;

    public OrderController(ProductsService productsService, OrderService orderService) {
        this.productsService = productsService;
        this.orderService = orderService;
    }

    @GetMapping("/order")
    public String createOrder(Model model) {
        model.addAttribute("order", new Order());
        Product[] products = this.productsService.getProducts();
        model.addAttribute("articles", products);
        return "order";
    }

    @PostMapping("/order")
    public String sendOrder(@RequestParam Map<String,String> allRequestParams, @RequestParam String address,
                            @ModelAttribute Order order, Model model) {
        allRequestParams.remove("address");
        System.out.println(order.getAddress());
        for (Map.Entry<String, String> param : allRequestParams.entrySet()) {
            order.addItem(new OrderItem(param.getKey(), Integer.valueOf(param.getValue())));
        }

        Order createdOrder = this.orderService.createOrder(order);
        model.addAttribute("createdOrder", createdOrder);

        return "orderCreated";
    }

}
