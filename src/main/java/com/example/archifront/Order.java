package com.example.archifront;

import java.util.ArrayList;
import java.util.List;

public class Order {
    private String id;
    private String address;
    private List<OrderItem> items = new ArrayList<>();


    public Order() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public List<OrderItem> getItems() {
        return items;
    }

    public void setItems(List<OrderItem> items) {
        this.items = items;
    }

    public void addItem(OrderItem orderItem) {
         this.items.add(orderItem);
    }

    @Override
    public String toString() {
        String order = "";

        order += String.format("ORDER ID : %s\n", this.getId());
        order += String.format("ADDRESSE : %s\n\n", this.getAddress());
        for (OrderItem item:
             items) {
            order += String.format("%s : %s\n", item.getProduct().id, item.getQuantity());
        }

        return order;
    }
}
