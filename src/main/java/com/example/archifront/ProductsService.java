package com.example.archifront;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ProductsService {
    private final RestTemplate restTemplate;

    public ProductsService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Product[] getProducts() {
        return this.restTemplate.getForObject("http://93.11.120.71:3769/api/Products", Product[].class);
    }
}
