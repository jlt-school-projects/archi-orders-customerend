package com.example.archifront;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class OrderService {
    private final RestTemplate restTemplate;

    public OrderService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    public Order createOrder(Order order) {
        return this.restTemplate.postForObject("http://93.11.120.71:3770/api/Orders", order, Order.class);
    }

    public Order[] getNotDoneOrders() {
        return this.restTemplate.getForObject("http://93.11.120.71:3770/api/Orders", Order[].class);
    }
}
